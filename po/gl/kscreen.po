# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2019-04-14 09:10+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: daemon.cpp:125
msgid "Switch Display"
msgstr "Cambiar de pantalla"

#~ msgid "Switch to external screen"
#~ msgstr "Cambiar a unha pantalla externa"

#~ msgid "Switch to laptop screen"
#~ msgstr "Cambiar á pantalla de portátil"

#~ msgid "Unify outputs"
#~ msgstr "Unificar as saídas"

#~ msgid "Extend to left"
#~ msgstr "Estender á esquerda"

#~ msgid "Extend to right"
#~ msgstr "Estender á dereita"

#~ msgid "Leave unchanged"
#~ msgstr "Deixar sen cambiar"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "No External Display"
#~ msgstr "Non hai pantallas externas"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "Changing Screen Layout"
#~ msgstr "Cambiando a disposición da pantalla"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "Cloned Display"
#~ msgstr "Non hai pantallas externas"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "External Only"
#~ msgstr "Non hai pantallas externas"
